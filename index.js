/*1)оголошення змінних відбувається за допомогою ключивих слів var let const*/
/*2)prompt дозволяє користувачу вводити текст або число, а confirm використовується для підтвердження користувачем певної дії*/
/*3)змінна або значення одного типу автоматично перетворюється в інший тип. Наприклад, якщо ми додаио рядок до числа, то число буде автоматично перетворено в рядок*/

const name = "Konstiantyn";
const admin = name;
console.log(admin);

const days = 7;
const secondsInDay = 24 * 60 * 60;
const seconds = days * secondsInDay;
console.log(seconds);

let userNumber = prompt("Введите любое значение:");
console.log(Number(userNumber));